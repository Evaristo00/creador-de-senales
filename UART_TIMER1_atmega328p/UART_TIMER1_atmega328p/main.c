#include "Uart.h"
#include "timer1.h"

uint16_t comprobante;
char noValido[] = "Comando no valido \r";
char inicio[] = "Generador de se�ales digitales programable \r Ingrese frecuencia entre 100 y 10000Hz \r ON: para encender, OFF para apagar, RST para reiniciar \r";

int main(void)
{
    init_Uart();
	imprimirMensaje(inicio);
	sei();
    while (1) 
    {
		if (ingresoString()){					//informa si se apreto enter
			comprobante = verificarDato();
			switch(comprobante){
				case(ERROR):
					imprimirMensaje(noValido);
				break;
				case(RST):
					imprimirMensaje(inicio);
					resetTimer1();
				break;
				case(ON):
					onTimer1();
				break;
				case(OFF):
					offTimer1();
				break;
				default:
					setFrecuencia(comprobante);
				break;
			}
		}
    }
}

