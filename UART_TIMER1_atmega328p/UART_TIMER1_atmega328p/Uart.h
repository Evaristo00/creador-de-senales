/*
 * Uart.h
 *
 * Created: 6/7/2021 3:54:50 PM
 *  Author: lenovo
 */ 


#ifndef UART_H_
#define UART_H_
#define BR9600 (0x67)	// 0x67=103 configura BAUDRATE=9600@16MHz

#define  ERROR 0
#define  RST 1
#define  ON 2
#define  OFF 3

#include "serialPort.h"
#include <string.h>
#include <math.h>
#include <avr/interrupt.h>

void init_Uart();
uint8_t ingresoString();
uint16_t verificarDato();
void imprimirMensaje(char *);


#endif /* UART_H_ */