#include "timer1.h"

static uint16_t contador = (1000000UL/100) -1;

void onTimer1(){
	DDRB |= (1<<PB1);						//PB1 Salida
	TCCR1A=  (1<< COM1A0);					//COM1A  = Toggle
	TCCR1B= (1<<WGM12) |  (1<<CS11);		//CTC 8 prescalar
	OCR1A = contador;			//pongo la frecuencia
} 

void offTimer1(){
	DDRB &=~ (1<<PB1);
}

void resetTimer1(){
	contador = (1000000UL/100) -1;
	OCR1A = contador;			//pongo la frecuencia
}

void setFrecuencia(uint16_t f){
		contador = (1000000UL/f) -1;			
		OCR1A = contador;			//pongo la frecuencia
}