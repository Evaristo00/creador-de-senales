
#include "Uart.h"

static volatile char RX_Buffer=0;
static volatile char buffer[255];
static volatile char bufferTx[255];
static volatile uint8_t finPalabra = 0;		//1 si se llego un \r 0 caso contrario
static volatile uint8_t posRx= 0;
static volatile uint8_t posTx = 0, posImp=0;

//funciones intenernas 

uint16_t modoDigito();
uint8_t esDigito(char dato);
uint8_t modoCaracter();


void init_Uart(){
	SerialPort_Init(BR9600); 		// Inicializo formato 8N1 y BAUDRATE = 9600bps
	SerialPort_TX_Enable();			// Activo el Transmisor del Puerto Serie
	SerialPort_RX_Enable();			// Activo el Receptor del Puerto Serie
	SerialPort_RX_Interrupt_Enable();	// Activo Interrupción de recepcion.
}

uint8_t ingresoString(){
	if(finPalabra){
		SerialPort_RX_Interrupt_Enable();
		finPalabra =0;
		return 1;
	}
	return 0;
}

/*
0 no valido
1 RST
2 ON
3 OFF
entre 100-10000 frecuencia
*/
uint16_t verificarDato(){
	volatile uint16_t res= 0;
	volatile unsigned long aux = atoi(buffer);
	if (aux >=100 && aux <= 10000){
		res = aux;
	}
	else{
		res = modoCaracter();
	}
	
	return res;
}

/*
0 no valido
1 RST
2 ON
3 OFF
*/
uint8_t modoCaracter(){
	volatile uint8_t pos= 0;
	if (strcmp(buffer,"RST")== 0){
		return 1;
	}
	else if (strcmp(buffer,"ON") == 0){
			return 2;
		}
		else if (strcmp(buffer,"OFF") == 0){
				return 3;
				}
			else
				return 0;
}

void imprimirMensaje(char * dato){	
	uint8_t i=0;
	while (dato[i] != '\0' ){
		bufferTx[posImp++] = dato[i];
		i++;
	}
	bufferTx[posImp++] = '\0';
	SerialPort_TX_Interrupt_Enable();
}

// Rutina de Servicio de Interrupción de Byte Recibido
ISR(USART_RX_vect){
	RX_Buffer = UDR0; //la lectura del UDR borra flag RXC
	if (RX_Buffer == '\r' ){
		buffer[posRx]= '\0';
		posRx = 0;
		finPalabra = 1;
		SerialPort_RX_Interrupt_Disable();	
	}else{
		buffer[posRx++]= RX_Buffer;
	}
}

ISR (USART_UDRE_vect){				//interrupe cuando el buffer este libre.
	if (posTx < posImp ){
		SerialPort_Send_Data(bufferTx[posTx++]);
	}else{
		SerialPort_TX_Interrupt_Disable();
		posImp= 0;
		posTx= 0;
	}
}